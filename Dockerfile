FROM gitlab/gitlab-runner:latest

RUN true \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get clean && apt-get purge && rm -r /var/lib/apt/lists/* \
    && true

RUN true \
    && apt-get update \
    && apt-get install -y software-properties-common \
    && apt-add-repository --yes --update ppa:ansible/ansible \
    && apt-get install -y ansible \
    && apt-get clean && apt-get purge && rm -r /var/lib/apt/lists/* \
    && true

COPY .ssh_askpass /home/gitlab-runner/.ssh_askpass

RUN chown gitlab-runner:gitlab-runner /home/gitlab-runner/.ssh_askpass \
    && chmod 0755 /home/gitlab-runner/.ssh_askpass

ENV SSH_ASKPASS=/home/gitlab-runner/.ssh_askpass
